import os

from src.data_reader import DataReader, SlayerParams
from src.slayer_train import SlayerTrainer, SpikeFunc, SpikeLinear, SpikeConv2d, SpikePool2d

import torch.nn as nn
import torch
import unittest
from torch.utils.data import DataLoader
from datetime import datetime

CURRENT_TEST_DIR = os.path.dirname(os.path.realpath(__file__))
# Speedup if input size is constant
torch.backends.cudnn.benchmark = True

class NMNISTNet2(nn.Module):

    #输入为 HxWxCxT,之后每一个特征图都以此格式顺序表示
    #C为channel，此处为2,为此NMNIST训练集的特殊之处，一般灰度图为1，彩图为3
    #T为Spike的时间跨度
    #初始化网络结构（Net）：34x34x1x2x1-12c5-2a-64c5-2a-10o

    def __init__(self, net_params, weights_init = [0.5,1,1], device=torch.device('cpu')):
        super(NMNISTNet2, self).__init__()
        self.net_params = net_params
        self.trainer = SlayerTrainer(net_params, device)
        self.input_srm = self.trainer.calculate_srm_kernel()
        self.srm = self.trainer.calculate_srm_kernel(1)
        self.ref = self.trainer.calculate_ref_kernel()
        # 34x34x2xT --(12c5)--> 34x34x12xT
        self.conv2d_1 = SpikeConv2d(net_params['input_channels'], 12, 5).to(device)
        nn.init.normal_(self.conv2d_1.weight, mean=0, std=weights_init[0])
        # 34x34x12xT --(2a)--> 17x17x12xT
        self.a1 = SpikePool2d(in_channels = 12, kernel_size = 2, theta = net_params['af_params']['theta']).to(device)# SpikePool2d类实例化时已经初始化好权重
        # Emulate a cnn layer 17x17x12xT --(64c5)--> 17x17x64xT
        self.conv2d_2 = SpikeConv2d(12, 64, kernel_size=5).to(device)
        nn.init.normal_(self.conv2d_2.weight, mean=0, std=weights_init[1])
        # 17x17x64xT --(2a)--> 16x16x64xT

        self.a2 = SpikePool2d(in_channels = 64, kernel_size = 2, theta = net_params['af_params']['theta']).to(device)
        # 16x16x64xT --(Dense)--> 10xT
        self.fc1 = SpikeLinear(8*8*64, 10).to(device)

        nn.init.normal_(self.fc1.weight, mean=0, std=weights_init[2])
        self.device=device

    #前向传播
    #TODO：这里建议，既然以后都是srm+layer+SpikeFunc，那么就把这个作为一个基本结构，写成类
    #TODO：或者像下面这样，简写成一个基本layer如spike_12c5
    #TODO：暂时不清楚apply_srm_kernel这个是怎么计算，参数该填什么，SpikeFunc.apply的参数应该填什么
    #TODO: SpikeFunc.apply(参数填什么)
    #TODO: 另外，这样，定义结构和forwar的时候，都要手动的填写一个个维度，建议还是形成一个规范，用将网络结构和超参数定义在cfg文本文件中
    #TODO：用封装的类读取cfg文件，主要目的是方便修改，不用手动一个个填维度，以及跨语言、跨框架。毕竟文本文件不受环境、框架、编程语言的限制。

    def forward(self, x):
        # spike_12c5
        x = self.trainer.apply_srm_kernel(x, self.input_srm)

        x = x.reshape((self.net_params['batch_size'], 2, 34, 34, -1))
        x = self.conv2d_1(x)
        x = SpikeFunc.apply(x, self.net_params, self.ref, self.net_params['af_params']['sigma'][0], self.device)
        # spike_2a
        x = self.trainer.apply_srm_kernel(x.view(self.net_params['batch_size'], 12, 1, 34*34, -1), self.trainer.calculate_srm_kernel(12))
        x = x.reshape((self.net_params['batch_size'], 12, 34, 34, -1))
        x = self.a1(x)
        x = SpikeFunc.apply(x, self.net_params, self.ref, self.net_params['af_params']['sigma'][1], self.device)
        # spike_64c5
        x = self.trainer.apply_srm_kernel(x.view(self.net_params['batch_size'],12, 1, 17*17, -1), self.trainer.calculate_srm_kernel(12))
        x = x.reshape((self.net_params['batch_size'], 12, 17, 17, -1))
        x = self.conv2d_2(x)
        x = SpikeFunc.apply(x, self.net_params, self.ref, self.net_params['af_params']['sigma'][2], self.device)
        # spike_2a
        x = self.trainer.apply_srm_kernel(x.view(self.net_params['batch_size'], 64, 1, 17*17, -1), self.trainer.calculate_srm_kernel(64))
        x = x.reshape((self.net_params['batch_size'], 64, 17, 17, -1))
        x = self.a2(x)
        x = SpikeFunc.apply(x, self.net_params, self.ref, self.net_params['af_params']['sigma'][3], self.device)
        # spike_10o
        x = self.trainer.apply_srm_kernel(x.view(self.net_params['batch_size'], 64, 1, 8*8, -1), self.trainer.calculate_srm_kernel(64))
        x = x.reshape((self.net_params['batch_size'], 1, 1, 64*8*8, -1))

        x = self.fc1(x)
        x = SpikeFunc.apply(x, self.net_params, self.ref, self.net_params['af_params']['sigma'][3], self.device)
        return x

class TestNMNISTTraining(unittest.TestCase):

    def setUp(self):
        self.FILES_DIR = CURRENT_TEST_DIR + "/../test/test_files/NMNISTsmall/"
        self.net_params = SlayerParams(CURRENT_TEST_DIR + "/../test/test_files/NMNISTsmall/" + "parameters.yaml")
        # self.net_params['batch_size'] = 20
        self.cuda = torch.device('cuda')
        self.reader = DataReader(self.FILES_DIR, "train.txt", self.net_params, self.cuda)
        self.test_reader = DataReader(self.FILES_DIR, "test.txt", self.net_params, self.cuda, file_offset=60001)
        self.trainer = SlayerTrainer(self.net_params, self.cuda)
        self.net = NMNISTNet2(self.net_params, device=self.cuda)

        self.train_loader = DataLoader(dataset=self.reader, batch_size=self.net_params['batch_size'], shuffle=False)
        self.test_loader = DataLoader(dataset=self.test_reader, batch_size=self.net_params['batch_size'], shuffle=False)

        self.optimizer = torch.optim.Adam(self.net.parameters(), lr=0.01)

    def test_nmnist_train(self):
        # Needed for CUDA allocation to work in DataLoader
        torch.multiprocessing.set_start_method("spawn")

        for epoch in range(100):

            correct_classifications = 0
            training_loss = 0
            epoch_t0 = datetime.now()
            for i, data in enumerate(self.train_loader, 0):
                t0 = datetime.now()
                self.optimizer.zero_grad()
                minibatch, des_spikes, labels = data
                minibatch = minibatch.reshape(self.net_params['batch_size'],2,1,34*34,350)
                output = self.net(minibatch)
                correct_classifications += self.trainer.get_accurate_classifications(output, labels)
                loss = self.trainer.calculate_l2_loss_classification(output, des_spikes)
                training_loss += loss.data
                loss.backward()
                self.optimizer.step()
                print(i, ":", (datetime.now() - t0).total_seconds())
            print("Epoch n.", epoch)
            print("Epoch time", (datetime.now() - epoch_t0).total_seconds())
            epoch_t0 = datetime.now()
            print("Training accuracy: ", correct_classifications / (len(self.train_loader) * self.net_params['batch_size']))
            print("Training loss: ", training_loss.data / (len(self.train_loader) * self.net_params['batch_size']))
            correct_classifications = 0
            testing_loss = 0
            for i, data in enumerate(self.test_loader, 0):
                minibatch, des_spikes, labels = data
                minibatch = minibatch.reshape(self.net_params['batch_size'],2,1,34*34,350)
                output = self.net(minibatch)
                correct_classifications += self.trainer.get_accurate_classifications(output, labels)
                testing_loss += self.trainer.calculate_l2_loss_classification(output, des_spikes).data
            print("Testing accuracy: ", correct_classifications / (len(self.test_loader) * self.net_params['batch_size']))
            print("Testing loss: ", testing_loss.data / (len(self.test_loader) * self.net_params['batch_size']))
