# Add to path
import sys, os

CURRENT_TEST_DIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(CURRENT_TEST_DIR + "/../src")

from data_reader import DataReader, SlayerParams
from testing_utilities import iterable_float_pair_comparator, iterable_int_pair_comparator, is_array_equal_to_file
from slayer_train import SlayerTrainer, SpikeFunc, SlayerNet, SpikeLinear

import os
from itertools import zip_longest
import numpy as np
from tensorpack import *
from tensorpack.tfutils.symbolic_functions import prediction_incorrect
from tensorpack.tfutils.summary import add_moving_summary, add_param_summary
from tensorpack.dataflow import dataset
from tensorpack.tfutils.varreplace import remap_variables
import tensorflow as tf

import argparse
import pprint

from tensorpack.tfutils.varmanip import get_checkpoint_path
import numpy as np
import math
import os
import time
#


tf.enable_eager_execution()
import numpy as np
import torch
import scipy
# def data_load():
# net_params = SlayerParams(CURRENT_TEST_DIR + "/test_files/NMNISTsmall/" + "parameters.yaml")
# trainer = SlayerTrainer(net_params)
# reader = DataReader(CURRENT_TEST_DIR + "/test_files/NMNISTsmall/", "train1K.txt", net_params)
#
# data = reader.read_and_bin_input_file(1)
# print(data)
    #return data

# ss = data_prepro
# print(ss)
def conv_ltc(input, w, **kwargs):
    name = 'myConvLayer'
    if 'name' in kwargs:
        name = kwargs['name']
    if 'strides' in kwargs:
        strides = kwargs['strides']
    with tf.name_scope(name):
        input =np.array(input)

        #print('convinput is', input.shape)
        #b = b * 1/255.
        out = tf.nn.conv2d(input, w, strides = (1,strides,strides,1), padding ='VALID')
        #print("conv out is", out.shape)

        return out

def eps_func(mult, tau, epsilon):
    # eps_func = []
    # for t in np.arange(0, t_end, t_s):
    srm_val = mult * t / tau * tf.math.exp(1 - t / tau)
    return srm_val

def psp(net_params):
    psp = eps_func(net_params['ref_params']['mult'], net_params['ref_params']['tau'],
                                          net_params['ref_params']['epsilon'], net_params['t_end'],
                                          net_params['t_s'])
    return psp

def ref_kernel(net_params):
    ref_kernel = eps_func(net_params['ref_params']['mult'], net_params['ref_params']['tau'],
                                          net_params['ref_params']['epsilon'], net_params['t_end'],
                                          net_params['t_s'])
    return ref_kernel
if __name__ == '__main__':
    out =[]
    psp = psp()
    ref = ref_kernel()
    w0 =[]
    inputs = np.ones((350,34,34,2))
    t =0
    for input in inputs:
        out1 = tf.multiply(psp,input)
        accum_out1 = out1 if t < 1 else tf.add(accum_out1, out1)
        out1_2 = conv_ltc(accum_out1,w0, trides=1, name='conv0')
        sout1 = tf.clip_by_value(tf.sign(tf.add(out1_2, -1)), 0, 1)
        out1_3 = tf.multiply(ref,out1_2)

        t += 1

    out = out.append(out)
