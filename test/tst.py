# Add to path
import sys, os

CURRENT_TEST_DIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(CURRENT_TEST_DIR + "/../src")
import numpy as np
from tensorpack import *
from tensorpack.tfutils.symbolic_functions import prediction_incorrect
from tensorpack.tfutils.summary import add_moving_summary, add_param_summary
from tensorpack.dataflow import dataset
from tensorpack.tfutils.varreplace import remap_variables
import tensorflow as tf

import argparse
import pprint

from tensorpack.tfutils.varmanip import get_checkpoint_path
import numpy as np
import math
import os
import time
#


#tf.enable_eager_execution()
from data_reader import DataReader, SlayerParams
from testing_utilities import iterable_float_pair_comparator, iterable_int_pair_comparator, is_array_equal_to_file
from slayer_train import SlayerTrainer, SpikeFunc, SlayerNet, SpikeLinear

import os
from itertools import zip_longest
import numpy as np
import torch

def read_gtruth_folder(folder):
		gtruth = {}
		for file in os.listdir(folder):
			if file.endswith('.csv'):
				gtruth[file[0:-4]] = torch.from_numpy(np.genfromtxt(folder + file, delimiter=",", dtype=np.float32))
		return gtruth

def process_event(raw_bytes):
	ts = int.from_bytes(raw_bytes[2:], byteorder='big') & 0x7FFFFF
	return (raw_bytes[0], raw_bytes[1], raw_bytes[2] >> 7, ts)





if __name__ == '__main__':
    # event =reader.read_and_bin_input_file(1)
    # print(event.shape)
	net_params = SlayerParams(CURRENT_TEST_DIR + "/test_files/NMNISTsmall/" + "parameters.yaml")
	trainer = SlayerTrainer(net_params)
	reader = DataReader(CURRENT_TEST_DIR + "/test_files/NMNISTsmall/", "train.txt", net_params)
	print(reader[0][0].shape)
    #print (inp)