This package is a Pytorch port of the original Spike LAYer Error Reassignment framework for backpropagation based spiking neural networks (SNNs) learning.

## Requirements
Python 3 with the following packages installed:

* pytorch, tested with version 0.4.1
* numpy
* pyyaml
* unittest

A CUDA enabled GPU is required for training any model.
The software has been tested with CUDA libraries version 9.2 and GCC 7.3.0 on Ubuntu 18.04

## Installation
The repository includes C++ and Cuda code that has to be compiled and installed before it can be used from Python, download the repository and run the following command to do so:

`python setup.py install`

## Examples
An example of how to train the network for classification purposes, on the NMNIST dataset, can be found in the nminst_net.py file. Make sure to change the FILES_DIR variable to indicate the folder where you downloaded the NMNIST dataset.

A .yaml file is used to load and customise most parameters (i.e. neuron thresholds), an example can be found under test/test_files/NMNISTsmall/parameters.yaml. This file will also be used for the NMNIST example code.

## Contact
For queries contact Luca lucadellavr@gmail.com

# SNN基本知识、问题汇总  
## 1、核心的部分：slayer_train.py文件  
--dylanhooz 2019/3/23

1、SNN和传统的深度神经网络，核心的区别就是 input的处理（srm）--->layer---->激活（SpikeFunc）,spike神经网络需要加入：input处理和激活，
这两个操作，在Slayer_train.py中即：

* input处理：class SlayerTrainer(object)
* 激活：class SpikeFunc(torch.autograd.Function)

2、一个完整的SNN的结构定义，案例参照class SlayerNet(nn.Module)，这个class不包含loss部分的计算

3、SNN由于输入输出都只有0、1两种值，因此传统的DNN的各个layer、激活函数、各种操作，都要做出一定改变：

* 激活函数统一使用SpikeFunc一种，代替传统的sigmoid、relu等各种激活函数
* layer：包括dense、pool、conv等，都需要重新定义Spike专用的layer，在slayer_train.py中定义了各种新的layer，统一用SpikeXxx定义，因为多了一个维度T=350s，基本目前继承的nn.Conv3d类
* nn.Conv3d父类，必须严格执行input的shape为(N, C_{in}, D_{in}, H_{in}, W_{in})，否则前后向传播出错，我们将其理解为(N, C_{in}, H_{in}, W_{in}, T_{in})
	* N：batch
	* C：channel
	* H：height
	* W：width
	* T：time=350s

## 2、需要解决的问题  
--dylanhooz 2019/3/23

1、BatchNormal等传统的正则化操作，在Spike中应该怎么定义此layer

2、Spike计算loss应该如何计算

	